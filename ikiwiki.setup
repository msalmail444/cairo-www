#!/usr/bin/perl
# Configuration file for ikiwiki.
# Passing this to ikiwiki --setup will make ikiwiki generate wrappers and
# build the wiki.
#
# Remember to re-run ikiwiki --setup any time you edit this file.

# The gitrepo parameter should point to a bare git repository which is
# cloned from and pushed to by anyone using git to maintain the wiki.
#
# The checkout parameter should point to a clone of that repository,
# but this clone is owned by ikiwiki so its working-copy content
# should never be edited manually, nor should anybody clone from or
# push to this repository. To make this quite clear, I suggest hiding
# the checkout inside the repository as show below.
#
# To set this up, just do:
#
#	mkdir -p $gitrepo
#	cd $gitrepo
#	GIT_DIR=. git init-db
#	git clone $gitrepo ikiwiki-checkout
#
# And then push into $gitrepo this ikiwiki.setup file, a templates/
# directory, and a src/ directory with your wiki content.
#
# Also, see the destdir and url parameters below which must also be
# configured before things will work at all.

our ($gitrepo, $checkout);

BEGIN {
    $gitrepo = '/srv/cairo.freedesktop.org/wiki';
    $checkout = "$gitrepo/ikiwiki-checkout";
}

use IkiWiki::Setup::Standard {

        # These (along with gitrepo above) must be configured
        # correctly before anything will work
	destdir => "/srv/cairo.freedesktop.org/www",
	url => "http://cairographics.org/",
	#cgiurl => "http://cairographics.org/ikiwiki",

	# And you'll likely want to customise these as well
	wikiname => "cairographics.org",
	adminuser => ["cworth", ],
	adminemail => 'cworth@cairographics.org',

	# Everything else can be customised on an as-needed basis
	srcdir => "$checkout/src",
	templatedir => "$checkout/templates",
	underlaydir => "/dev/null",

        # force ikiwiki to use a particular umask
	umask => 022,

	# Git stuff.
	rcs => "git",
	#historyurl => "http://git.example.org/gitweb.cgi?p=wiki.git;a=history;f=[[file]]",
	#diffurl => "http://git.example.org/gitweb.cgi?p=wiki.git;a=blobdiff;h=[[sha1_to]];hp=[[sha1_from]];hb=[[sha1_parent]];f=[[file]]",
	gitorigin_branch => "origin",
	gitmaster_branch => "master",

	wrappers => [
#		{
#			# The cgi wrapper.
#			cgi => 1,
#			wrapper => "/srv/cairo.freedesktop.org/cgi-bin/ikiwiki",
#			wrappermode => "06755",
#		},
		{
			# Instead of overwriting git's post-update script
		        # we generate a new program that can be called by
		        # post-update as desired.
			wrapper => "$gitrepo/hooks/ikiwiki-post-update",
			wrappermode => "04755",
			# Enable mail notifications of commits.
			notify => 1,
		},
	],

	# Generate rss feeds for blogs?
	rss => 1,
	# Generate atom feeds for blogs?
	atom => 1,
	# Urls to ping with XML-RPC when rss feeds are updated
	#pingurl => [qw{http://rpc.technorati.com/rpc/ping}],
	# Include discussion links on all pages?
	discussion => 0,
	# To exclude files matching a regexp from processing. This adds to
	# the default exclude list.
	#exclude => qr/*\.wav/,
	# Time format (for strftime)
	#timeformat => '%c',
	dateformat => '%F',
	# Locale to use. Must be a UTF-8 locale.
	#locale => 'en_US.UTF-8',
	# Only send cookies over SSL connections.
	#sslcookie => 1,
	# Logging settings:
	syslog => 0,
	# To link to user pages in a subdirectory of the wiki.
	#userdir => "users",
	# To create output files named page.html rather than page/index.html.
	#usedirs => 0,
	# Simple spam prevention: require an account-creation password.
	#account_creation_password => "example",

	# To add plugins, list them here.
	#add_plugins => [qw{goodstuff search wikitext camelcase
	#                   htmltidy fortune sidebar map rst anonok}],
	add_plugins => [qw{goodstuff pagetemplate}],
	# If you want to disable any of the default plugins, list them here.
	#disable_plugins => [qw{inline htmlscrubber passwordauth openid}],
	disable_plugins => [qw{smiley recentchanges}],

	# For use with the tag plugin, make all tags be located under a
	# base page.
	#tagbase => "tag",

	# For use with the search plugin if your estseek.cgi is located
	# somewhere else.
	#estseek => "/usr/lib/estraier/estseek.cgi",

	# For use with the openid plugin, to give an url to a page users
	# can use to signup for an OpenID.
	#openidsignup => "http://myopenid.com/",

	# For use with the mirrorlist plugin, a list of mirrors.
	#mirrorlist => {
	#	mirror1 => "http://hostname1",
	#	mirror2 => "http://hostname2/mirror",
	#},
}
