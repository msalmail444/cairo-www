[[!meta title="Help on formatting wiki pages"]]

Text on this wiki is, by default, written in a form very close to how you
might write text for an email message. This style of text formatting is
called [MarkDown](http://daringfireball.net/projects/markdown/), and it
works like this:

Leave blank lines between paragraphs.

You can \**emphasise*\* or \*\***strongly emphasise**\*\* text by placing it
in single or double asterisks.

To create a list, start each line with an asterisk:

* "* this is my list"
* "* another item"

To make a numbered list, start each line with a number (any number will
do) followed by a period:

1. "1. first line"
2. "2. second line"
2. "2. third line"

To create a header, start a line with one or more `#` characters followed
by a space and the header text. The number of `#` characters controls the
size of the header:

# # h1
## ## h2
### ### h3
#### #### h4
##### ##### h5
###### ###### h6

To create a horizontal rule, just write three or more dashes or stars on
their own line:

----

To quote someone, prefix the quote with ">":

> To be or not to be,
> that is the question.

To write a code block, indent each line with a tab or 4 spaces:

	10 PRINT "Hello, world!"
	20 GOTO 10

To link to an url or email address, you can just put the
url in angle brackets: <<http://ikiwiki.info>>, or you can use the
form \[link text\]\(url\)

----

In addition to basic html formatting using
[MarkDown](http://daringfireball.net/projects/markdown/), this wiki lets
you use the following additional features:

* To link to another page on the wiki, place the page's name inside double
  square brackets. So you would use `\[[News]]` to link to [[News]].

[[!if test="enabled(shortcut) and shortcuts" then="""
* Use [[shortcuts]] to link to common resources.

	\[[wikipedia War\_of\_1812]]
"""]]

[[!if test="enabled(toc)" then="""
* Add a table of contents to a page:

	\[[toc ]]
"""]]

[[!if test="enabled(meta)" then="""
* Change the title of a page:

	\[[meta title="full page title"]]
"""]]

