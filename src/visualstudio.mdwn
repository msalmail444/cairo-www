For Microsoft VS.net users, before building the Cairo library, you need to create a header file called "cairo-features.h",
create a Win32 DLL project, and then add the requested source files into the project.

You can get the Win32 project from there: [The MSVC project of the cairo library](http://slinavlee.googlepages.com/)

cairo-features.h for msvc using win32 font:

    #ifndef CAIRO_FEATURES_H
    #define CAIRO_FEATURES_H

    #define CAIRO_HAS_IMAGE_SURFACE 1
    #define CAIRO_HAS_USER_FONT 1

    #define CAIRO_HAS_PNG_FUNCTIONS 1

    #define CAIRO_HAS_SVG_SURFACE 1
    #define CAIRO_HAS_PDF_SURFACE 1
    #define CAIRO_HAS_PS_SURFACE 1

    #define CAIRO_HAS_WIN32_SURFACE 1
    #define CAIRO_HAS_WIN32_FONT 1

    #endif
