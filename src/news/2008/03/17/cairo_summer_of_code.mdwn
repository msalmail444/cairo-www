[[!meta title="Cairo participates in Google Summer of Code 2008"]]
[[!meta date="2008-03-18"]]

Welcome, students!

The cairo graphics project is participating in [Google Summer of Code
2008](http://code.google.com/soc/2008/). This is a fantastic
opportunity for students to gain experience with the free-software
community, work closely with experts, and also make a valuable
contribution to a significant project. More than that, working with the
cairo graphics library and community is a lot of fun!  (And Google
even pays the student a little bit.)

Interested students should review [Google's
overview](http://code.google.com/soc/2008/) of the program and also
the [cairo project's ideas](http://cairographics.org/summerofcode/ideas/).
Each idea in this list contains a short description of what the
project would consist of, and also a difficulty rating (Easy, Medium,
or Hard), so that students can better estimate their ability to
complete the project.

Any student that sees an interesting project idea there should then
subscribe to the [cairo@cairographics.org mailing
list](http://cairographics.org/lists/) and send an email off stating
the project of interest. There on the list, the student can start to
interact with potential mentors and learn more details about what the
project will require.

Finally, between March 24 and March 31, (see [Google's
timeline](http://code.google.com/opensource/gsoc/2008/faqs.html#0.1_timeline)
for the exact cutoff dates/times) students will submit applications to
the Summer of Code program. And then the real fun starts as mentors
and students work together over the (northern hemisphere) summer to
make cairo more excellent than ever.

We're pleased that you're interested in the cairo graphics
library. And we hope that you enjoy playing with and improving this
code. Have fun with cairo!
